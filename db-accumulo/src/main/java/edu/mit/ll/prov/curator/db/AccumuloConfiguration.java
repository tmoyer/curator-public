package edu.mit.ll.prov.curator.db;

public final class AccumuloConfiguration {

    protected boolean mock = false;
    protected String instanceName;
    protected String zooserver;
    protected String username;
    protected String password;
    protected Integer batchBytes;
    protected Integer numThreads;

    public void setMock(boolean mock) {
        this.mock = mock;
    }

    public boolean getMock() {
        return mock;
    }

    public void setInstanceName(String instanceName) {
        this.instanceName = instanceName;
    }

    public String getInstanceName() {
        return instanceName;
    }

    public void setZooserver(String zooserver) {
        this.zooserver = zooserver;
    }

    public String getZooserver() {
        return zooserver;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUsername() {
        return username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPassword() {
        return password;
    }

    public void setBatchBytes(Integer batchBytes) {
        this.batchBytes = batchBytes;
    }

    public Integer getBatchBytes() {
        return batchBytes;
    }

    public void setNumThreads(Integer numThreads) {
        this.numThreads = numThreads;
    }

    public Integer getNumThreads() {
        return numThreads;
    }

}
