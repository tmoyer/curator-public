package edu.mit.ll.prov.curator.model;

import java.util.List;
import java.util.Map;
import javax.xml.namespace.QName;

public abstract class Vertex extends Attributes {

    protected final QName id;

    public Vertex(QName id) {
        this.id = id;
    }

    public Vertex(QName id, Map<QName, Object> attrs) {
        this.id = id;
        this.attrs = attrs;
    }

    public QName getId() {
        return id;
    }

    public abstract String getType();
    
    public String toString() {
        return toString(false);
    }
    
    public String toString(boolean sanitized) {
        String str = String.format("%s %s in %s%n", getType(), id.getLocalPart(), id.getNamespaceURI());
        if (!sanitized) {
            str += super.toString();
        }
        return str;
    }
}
