package edu.mit.ll.prov.curator.model;

import java.util.Map;

import javax.xml.namespace.QName;

public class ActedOnBehalfOf extends Edge {

    public ActedOnBehalfOf(QName id, Agent source, Agent destination) {
        super(id, source, destination);
    }

    public ActedOnBehalfOf(QName id, Agent source, Agent destination, Map<QName, Object> attrs) {
        super(id, source, destination, attrs);
    }

    public ActedOnBehalfOf(QName id, QName source, QName destination) {
        super(id, source, destination);
    }

    public ActedOnBehalfOf(QName id, QName source, QName destination, Map<QName, Object> attrs) {
        super(id, source, destination, attrs);
    }

    public String getType() {
        return "actedOnBehalfOf";
    }
}
