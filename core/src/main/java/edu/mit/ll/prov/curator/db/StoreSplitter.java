package edu.mit.ll.prov.curator.db;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import edu.mit.ll.prov.curator.model.Edge;
import edu.mit.ll.prov.curator.model.Graph;
import edu.mit.ll.prov.curator.model.Vertex;

public class StoreSplitter implements Store {
    protected List<Store> stores = new ArrayList<>();

    public StoreSplitter() {
    }

    public StoreSplitter(List<Store> stores) {
        this.stores = stores;
    }

    public void configure(Map config) {
        // the config describes other Stores - maybe have a factory class to create them?
        throw new UnsupportedOperationException("not implemented");
    }
    
    public void add(Store store) {
        stores.add(store);
    }
    
    public void write(Graph graph) throws StoreException {
        for (Store store : stores) {
            store.write(graph);
        }
    }
    
    public void write(Vertex vertex) throws StoreException {
        for (Store store : stores) {
            store.write(vertex);
        }
    }
        
    public void write(Edge edge) throws StoreException {
        for (Store store : stores) {
            store.write(edge);
        }
    }

}
