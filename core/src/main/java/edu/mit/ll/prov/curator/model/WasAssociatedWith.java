package edu.mit.ll.prov.curator.model;

import java.util.Map;

import javax.xml.namespace.QName;

public class WasAssociatedWith extends Edge {

    public WasAssociatedWith(QName id, Activity source, Agent destination) {
        super(id, source, destination);
    }

    public WasAssociatedWith(QName id, Activity source, Agent destination, Map<QName, Object> attrs) {
        super(id, source, destination, attrs);
    }

    public WasAssociatedWith(QName id, QName source, QName destination) {
        super(id, source, destination);
    }

    public WasAssociatedWith(QName id, QName source, QName destination, Map<QName, Object> attrs) {
        super(id, source, destination, attrs);
    }

    public String getType() {
        return "wasAssociatedWith";
    }
}
