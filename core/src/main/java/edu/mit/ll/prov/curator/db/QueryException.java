package edu.mit.ll.prov.curator.db;

public class QueryException extends DatabaseException {
    public QueryException(String message) {
        super(message);
    }

    public QueryException(Throwable cause) {
        super(cause);
    }

    public QueryException(String message, Throwable cause) {
        super(message, cause);
    }
}
