package edu.mit.ll.prov.curator.deserialize;

import java.util.Map;

import edu.mit.ll.prov.curator.model.Edge;
import edu.mit.ll.prov.curator.model.Graph;
import edu.mit.ll.prov.curator.model.Vertex;

public interface Deserializer {
    public void configure(Map config);
    public Graph deserialize(String content) throws DeserializeException;
}
