package edu.mit.ll.prov.curator.model;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import javax.xml.namespace.QName;

import edu.mit.ll.prov.curator.CuratorException;

public class Attributes {

    public Map<QName, Object> attrs = new HashMap<>();
    
    public String toString() {
        StringBuilder str = new StringBuilder();
        Set<Map.Entry<QName, Object>> attrSet = attrs.entrySet();
        for (Map.Entry<QName, Object> attr : attrSet) {
            str.append(String.format("    %s in %s: %s (type %s)%n", attr.getKey().getLocalPart(), attr.getKey().getNamespaceURI(),
                                 attr.getValue(), getAttributeType(attr.getKey()).getLocalPart()));
        }
        return str.toString();
    }

    public void set(Map<QName, Object> attributes) {
        attrs.putAll(attributes);
    }

    public void setAttribute(QName name, Object value) {
        attrs.put(name, value);
    }

    public void setAttribute(QName name, String value, QName type) {
        if ("boolean".equals(type.getLocalPart())) {
            attrs.put(name, Boolean.valueOf(value));
        } else if ("string".equals(type.getLocalPart())) {
            attrs.put(name, value);
        } else if ("int".equals(type.getLocalPart())) {
            attrs.put(name, Integer.valueOf(value));
        } else if ("long".equals(type.getLocalPart())) {
            attrs.put(name, Long.valueOf(value));
        } else if ("float".equals(type.getLocalPart())) {
            attrs.put(name, Float.valueOf(value));
        } else if ("double".equals(type.getLocalPart())) {
            attrs.put(name, Double.valueOf(value));
        } else {
            // just add it as a string
            attrs.put(name, value);
        }
    }
    
    public Set<QName> getAttributeNames() {
        return attrs.keySet();
    }

    public Object getAttributeValue(QName name) {
        return attrs.get(name);
    }
    
    public boolean containsAttribute(QName name) {
        return attrs.containsKey(name);
    }
    
    public QName getAttributeType(QName name) {
        String xs = "http://www.w3.org/2001/XMLSchema";
        if (attrs.get(name) instanceof Boolean) {
            return new QName(xs, "boolean");
        } else if (attrs.get(name) instanceof String) {
            return new QName(xs, "string");
        } else if (attrs.get(name) instanceof Integer) {
            return new QName(xs, "int");
        } else if (attrs.get(name) instanceof Long) {
            return new QName(xs, "long");
        } else if (attrs.get(name) instanceof Float) {
            return new QName(xs, "float");
        } else if (attrs.get(name) instanceof Double) {
            return new QName(xs, "double");
        } else {
            // just call it a string
            return new QName(xs, "string");
        }
    }

}
