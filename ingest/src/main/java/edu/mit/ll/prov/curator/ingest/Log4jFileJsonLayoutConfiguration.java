package edu.mit.ll.prov.curator.ingest;

public class Log4jFileJsonLayoutConfiguration {

    private String path;

    public void setPath(String path) {
        this.path = path;
    }

    public String getPath() {
        return path;
    }

}
