package edu.mit.ll.prov.curator.ingest;

import java.util.Map;

import edu.mit.ll.prov.curator.deserialize.Deserializer;
import edu.mit.ll.prov.curator.db.Store;

public abstract class Receiver {
    protected final Deserializer deser;
    protected final Store store;

    public Receiver(Deserializer deser, Store store) {
        this.deser = deser;
        this.store = store;
    }

    public abstract void run();

}
