package edu.mit.ll.prov.curator.ingest;

public class Log4jSocketConfiguration {

    private Integer port;

    public void setPort(Integer port) {
        this.port = port;
    }

    public Integer getPort() {
        return port;
    }

}
