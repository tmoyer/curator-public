package edu.mit.ll.prov.curator.ingest;

public class ReceiveConfiguration {

    private Log4jSocketConfiguration log4jSocket;
    private Log4jFileJsonLayoutConfiguration log4jFileJson;

    public void setLog4jSocket(Log4jSocketConfiguration log4jSocket) {
        this.log4jSocket = log4jSocket;
    }

    public Log4jSocketConfiguration getLog4jSocket() {
        return log4jSocket;
    }

    public void setLog4jFileJson(Log4jFileJsonLayoutConfiguration log4jFileJson) {
        this.log4jFileJson = log4jFileJson;
    }

    public Log4jFileJsonLayoutConfiguration getLog4jFileJson() {
        return log4jFileJson;
    }

}
