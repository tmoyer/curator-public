package edu.mit.ll.prov.curator.ingest;

import edu.mit.ll.prov.curator.db.AccumuloConfiguration;
import edu.mit.ll.prov.curator.db.SqlConfiguration;

public class StoreConfiguration {

    private SqlConfiguration sql;
    private AccumuloConfiguration accumulo;

    public void setSql(SqlConfiguration sql) {
        this.sql = sql;
    }

    public SqlConfiguration getSql() {
        return sql;
    }

    public void setAccumulo(AccumuloConfiguration accumulo) {
        this.accumulo = accumulo;
    }

    public AccumuloConfiguration getAccumulo() {
        return accumulo;
    }

}
