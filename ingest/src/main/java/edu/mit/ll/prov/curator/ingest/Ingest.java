package edu.mit.ll.prov.curator.ingest;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import java.util.Map;

import org.yaml.snakeyaml.Yaml;

import edu.mit.ll.prov.curator.CuratorException;
import edu.mit.ll.prov.curator.db.Accumulo;
import edu.mit.ll.prov.curator.db.DatabaseException;
import edu.mit.ll.prov.curator.db.SQL;
import edu.mit.ll.prov.curator.db.Store;
import edu.mit.ll.prov.curator.deserialize.ProvJsonDeserializer;
import edu.mit.ll.prov.curator.deserialize.Deserializer;

/**

This is a simple ingester that can be configured to receive provenance events and store them to a database.
Typically, Curator will be used with a distributed logging framework and this class is not needed, but this
class may be convenient for testing or other simple cases.

 */
public abstract class Ingest {
    Store store = null;
    Deserializer deser = null;
    Receiver receiver = null;
    IngestConfiguration config = null;

    public Ingest(String configFile) throws CuratorException {
        Yaml yaml = new Yaml();
        try {
            try(InputStream in = Files.newInputStream(Paths.get(configFile))) {
                config = yaml.loadAs(in, IngestConfiguration.class);
            }
        } catch (IOException e) {
            throw new CuratorException(e);
        }

        configureDeserializer(config.getDeserialize());
        configureStore(config.getStore());
    }

    final protected void configureDeserializer(DeserializeConfiguration config) throws CuratorException {
        if (config == null) {
            throw new CuratorException("config file doesn't contain a 'deserialize' section");
        }
        if (config.getProvJson() != null) {
            deser = new ProvJsonDeserializer();
        } else {
            throw new CuratorException("no deserializer specified");
        }
    }

    final protected void configureStore(StoreConfiguration config) throws CuratorException {
        if (config == null) {
            throw new CuratorException("config file doesn't contain a 'store' section");
        }
        if (config.getSql() != null) {
            store = new SQL(config.getSql());
        } else if (config.getAccumulo() != null) {
            store = new Accumulo(config.getAccumulo());
        } else {
            throw new CuratorException("no store specified");
        }
    }

    protected abstract void configureReceiver(ReceiveConfiguration config) throws CuratorException;

    public void run() {
        receiver.run();
    }

}
