
'use strict';


function createImageTable(elementId) {
    let xmlHttp = new XMLHttpRequest();
    xmlHttp.onreadystatechange = function() {
        if (xmlHttp.readyState === 4 && xmlHttp.status === 200) {
            document.getElementById(elementId).innerHTML = imageTable(xmlHttp.responseText);
        }
    }
    xmlHttp.open("GET", "../api/provenance", true);
    xmlHttp.send(null);
}

function imageTable(responseText) {
    let jso = JSON.parse(responseText);

    let cnt = 0;
    let out = "<table><tr><th>Category</th><th>Name</th></tr>";
    for (let category in jso) {
        if (!jso.hasOwnProperty(category)) {
            continue;
        }
        
        for (let i = 0; i < jso[category].length; i++) {
            out += "<tr>";
            out += "<td>" + category + "</td>";
            out += "<td><a href='../provenance/" + category + "/" + jso[category][i] + "/index.html'>" +
            jso[category][i] + "</a></td>";
            out += "</tr>";
            ++cnt;
        }
    }
    out += "</table>";
    out += "<p>The response contains "+cnt+" objects</p>";
    return out;
}
