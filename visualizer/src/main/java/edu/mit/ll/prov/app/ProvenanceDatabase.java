package edu.mit.ll.prov.app;

import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.Path;

import org.yaml.snakeyaml.Yaml;

import edu.mit.ll.prov.curator.db.Accumulo;
import edu.mit.ll.prov.curator.db.Query;
import edu.mit.ll.prov.curator.db.QueryException;
import edu.mit.ll.prov.curator.db.SQL;
import edu.mit.ll.prov.curator.ingest.IngestConfiguration;

public class ProvenanceDatabase {

    public static Query getQuery() throws QueryException {
    try {
        Path filePath = Paths.get("./curator.yml");
        String sysPathStr = System.getProperty("curator.config.file");
        if (sysPathStr != null) {
            Path sysPath = Paths.get(sysPathStr).normalize();
            if (sysPath != null && sysPath.isAbsolute()) {
                filePath = sysPath;
            }
        }
        
        try(InputStream in = Files.newInputStream(filePath)) {
        Yaml yaml = new Yaml();  
        IngestConfiguration config = yaml.loadAs(in, IngestConfiguration.class);

        if (config.getStore() == null) {
            throw new QueryException("no store section found in config file");
        }
        if (config.getStore().getSql() != null) {
            return new SQL(config.getStore().getSql());
        }
        if (config.getStore().getAccumulo() != null) {
            return new Accumulo(config.getStore().getAccumulo());
        }

        throw new QueryException("no store specified in config file");
        }
    } catch (Exception e) {
        throw new QueryException(e);
    }
    }

}
