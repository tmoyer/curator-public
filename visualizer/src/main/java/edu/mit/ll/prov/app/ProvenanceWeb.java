package edu.mit.ll.prov.app;

import org.owasp.encoder.Encode;

import javax.ws.rs.*;
import javax.ws.rs.core.*;

@Path("/")
public class ProvenanceWeb {

    public ProvenanceWeb() {
    }

    @Path("{imagecategory}/{imagename}/index.html")
    @GET
    @Produces(MediaType.TEXT_HTML)
    public String index(@PathParam("imagename") String imageName,
			@PathParam("imagecategory") String imageCategory) {
        
        // Sanitize RESTful inputs from user 
        imageName = ProvenanceService.sanitizeProvInput(imageName, ProvenanceService.SanitizeType.GENERIC);
        imageName = Encode.forHtmlContent(imageName);
        imageCategory = ProvenanceService.sanitizeProvInput(imageCategory, ProvenanceService.SanitizeType.GENERIC);
        imageCategory = Encode.forHtmlContent(imageCategory);
        
        String pattern = "<!DOCTYPE html>\n" +
            "  <head>\n" +
            "    <title>Provenance for Image %imageName in %imageCategory</title>\n" +
            "    <script type='text/javascript' src='../../imageProvenance.js'></script>\n" + 
            "    <script type='text/javascript' src='../../imageAnalysis.js'></script>\n" + 
            "    <script type='text/javascript' src='../../vis.min.js'></script>\n" + 
            "    <script type='text/javascript' src='../../auto-complete.min.js'></script>\n" + 
            "    <link href='../../vis.min.css' rel='stylesheet' type='text/css'/>\n" + 
            "    <link href='../../auto-complete.css' rel='stylesheet' type='text/css'/>\n" + 
            "    <link href='../../webapp.css' rel='stylesheet' type='text/css'/>\n" + 
            "    <meta charset='UTF-8'>\n" + 
            "  </head>\n" +
            "  <body id='body_container'>\n" +
            "    <div id='header'><h1>Image %imageCategory/%imageName</h1></div>\n" +
            "    <div id='menu'>\n" + 
            "       <div id='jsonGraph' class='submenu' onmousedown=\"toggleMenuStyle(this);createProvJsonGraph('%imageName','%imageCategory','contents')\">Graph (JSON)</div>\n" + 
            "       <!--<div class='submenu' onmousedown=\"toggleMenuStyle(this);createProvImage('%imageName','%imageCategory','contents')\">Graph (Image)</div>-->\n" + 
            "       <!--<div class='submenu' onmousedown=\"toggleMenuStyle(this);createAnalysis('%imageName','%imageCategory','contents')\">Analysis</div>-->\n" + 
            "       <div class='submenu' onmousedown=\"toggleMenuStyle(this);createProvJson('%imageName','%imageCategory','contents')\">JSON</div>\n" + 
            "       <!--<div class='submenu' onmousedown=\"toggleMenuStyle(this);createProvn('%imageName','%imageCategory','contents')\">Provn</div>-->\n" + 
            "       <div class='submenu' style='background-color:#C0392B;color:#FFFFFF;border-color:#E74C3C;' onclick=\"wipeDatabase('%imageName','%imageCategory','contents')\">Wipe DB</div>\n" + 
            "    </div>\n" +
            "    <div id='contents_wrapper'><div id='contents'></div></div>\n" +
            "\n" +
            "<script>toggleMenuStyle(document.getElementById('jsonGraph'));createProvJsonGraph('%imageName','%imageCategory','contents')</script>\n" + 
            "  </body>\n" +
            "</html>\n";
        return pattern.replace("%imageName",imageName).replace("%imageCategory",imageCategory);
    }
    
    @Path("{vertexid}/index.html")
    @GET
    @Produces(MediaType.TEXT_HTML)
    public String index(@PathParam("vertexid") String vertexId) {
        
        // Sanitize RESTful inputs from user 
        vertexId = ProvenanceService.sanitizeProvInput(vertexId, ProvenanceService.SanitizeType.VERTEXID);
        vertexId = Encode.forHtmlContent(vertexId);
        
        if (vertexId.indexOf(':') >= 0) {
            String[] splitStr = vertexId.split(":",2);
            // toss out the namespace? 
            //TODO: allow custom namespace 
            vertexId = splitStr[1];
        }
        String pattern = "<!DOCTYPE html>\n" +
            "  <head>\n" +
            "    <title>Provenance for vertex %vertexId</title>\n" +
            "    <script type='text/javascript' src='../imageProvenance.js'></script>\n" + 
            "    <script type='text/javascript' src='../imageAnalysis.js'></script>\n" + 
            "    <script type='text/javascript' src='../vis.min.js'></script>\n" + 
            "    <script type='text/javascript' src='../auto-complete.min.js'></script>\n" + 
            "    <link href='../vis.min.css' rel='stylesheet' type='text/css'/>\n" + 
            "    <link href='../auto-complete.css' rel='stylesheet' type='text/css'/>\n" + 
            "    <link href='../webapp.css' rel='stylesheet' type='text/css'/>\n" + 
            "    <meta charset='UTF-8'>\n" + 
            "  </head>\n" +
            "  <body id='body_container'>\n" +
            "    <div id='header'><h1>Vertex %vertexId</h1></div>\n" +
            "    <div id='menu'>\n" + 
            "       <div id='jsonGraph' class='submenu' onmousedown=\"toggleMenuStyle(this);createProvJsonGraph('%vertexId',null,'contents')\">Graph</div>\n" + 
            "       <div class='submenu' onmousedown=\"toggleMenuStyle(this);createProvJson('%vertexId',null,'contents')\">JSON</div>\n" + 
            "       <div class='submenu' style='background-color:#C0392B;color:#FFFFFF;border-color:#E74C3C;' onclick=\"wipeDatabase('%vertexId',null,'contents')\">Wipe DB</div>\n" + 
            "    </div>\n" +
            "    <div id='contents_wrapper'><div id='contents'></div></div>\n" +
            "\n" +
            "<script>toggleMenuStyle(document.getElementById('jsonGraph'));createProvJsonGraph('%vertexId',null,'contents')</script>\n" + 
            "  </body>\n" +
            "</html>\n";
        return pattern.replace("%vertexId",vertexId);
    }

}
