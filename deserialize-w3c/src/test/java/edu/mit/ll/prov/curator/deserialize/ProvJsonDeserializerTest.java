package edu.mit.ll.prov.curator.deserialize;

import java.io.StringReader;

import java.util.Random;
import java.util.Vector;

import javax.json.*;
import javax.xml.namespace.QName;

import edu.mit.ll.prov.curator.model.*;
import edu.mit.ll.prov.curator.serialize.*;

public class ProvJsonDeserializerTest {

    protected Random random = new Random(5494389);

    public void testActivity() {
        Activity activity = new Activity(new QName("http://www.ll.mit.edu/prov/test", "activity1"));
        Graph graph = new Graph();
        graph.add(activity);
        Serializer ser = new ProvJsonSerializer();

        String str = ser.serialize(activity);
        //System.out.println(str);

        Deserializer deser = new ProvJsonDeserializer();
        Graph graph2 = null;
        try {
            graph2 = deser.deserialize(str);
        } catch (DeserializeException e) {
            assert(false);
        }
        //System.out.println(graph2);

        assert(identicalGraphs(graph, graph2));
    }

    public void testAgent() {
        Agent agent = new Agent(new QName("http://www.ll.mit.edu/prov/test", "agent1"));
        Graph graph = new Graph();
        graph.add(agent);
        Serializer ser = new ProvJsonSerializer();

        String str = ser.serialize(agent);
        //System.out.println(str);

        Deserializer deser = new ProvJsonDeserializer();
        Graph graph2 = null;
        try {
            graph2 = deser.deserialize(str);
        } catch (DeserializeException e) {
            assert(false);
        }
        //System.out.println(graph2);

        assert(identicalGraphs(graph, graph2));
    }

    public void testEntity() {
        Entity entity = new Entity(new QName("http://www.ll.mit.edu/prov/test", "entity1"));
        entity.setAttribute(new QName("http://www.ll.mit.edu/prov/test2", "boolValue"), true);
        entity.setAttribute(new QName("http://www.ll.mit.edu/prov/test2", "intValue"), 5);
        entity.setAttribute(new QName("http://www.ll.mit.edu/prov/test2", "longValue"), 9l);
        entity.setAttribute(new QName("http://www.ll.mit.edu/prov/test2", "floatValue"), 7.3f);
        entity.setAttribute(new QName("http://www.ll.mit.edu/prov/test2", "doubleValue"), 9.175);
        Graph graph = new Graph();
        graph.add(entity);
        Serializer ser = new ProvJsonSerializer();

        String str = ser.serialize(entity);
        //System.out.println(str);

        Deserializer deser = new ProvJsonDeserializer();
        Graph graph2 = null;
        try {
            graph2 = deser.deserialize(str);
        } catch (DeserializeException e) {
            assert(false);
        }
        //System.out.println(graph2);

        assert(identicalGraphs(graph, graph2));
    }

    public void testBehalfOf() {
        ActedOnBehalfOf behalf = new ActedOnBehalfOf(new QName("http://www.ll.mit.edu/prov/test", "used1"),
                                                     new QName("http://www.ll.mit.edu/prov/test", "agent1"),
                                                     new QName("http://www.ll.mit.edu/prov/test", "agent2"));
        Graph graph = new Graph();
        graph.add(behalf);
        Serializer ser = new ProvJsonSerializer();

        String str = ser.serialize(behalf);
        //System.out.println(str);

        Deserializer deser = new ProvJsonDeserializer();
        Graph graph2 = null;
        try {
            graph2 = deser.deserialize(str);
        } catch (DeserializeException e) {
            assert(false);
        }
        //System.out.println(graph2);

        assert(identicalGraphs(graph, graph2));
    }

    public void testUsed() {
        Used used = new Used(new QName("http://www.ll.mit.edu/prov/test", "used1"),
                             new QName("http://www.ll.mit.edu/prov/test", "activity1"),
                             new QName("http://www.ll.mit.edu/prov/test", "entity1"));
        Graph graph = new Graph();
        graph.add(used);
        Serializer ser = new ProvJsonSerializer();

        String str = ser.serialize(used);
        //System.out.println(str);

        Deserializer deser = new ProvJsonDeserializer();
        Graph graph2 = null;
        try {
            graph2 = deser.deserialize(str);
        } catch (DeserializeException e) {
            assert(false);
        }
        //System.out.println(graph2);

        assert(identicalGraphs(graph, graph2));
    }

    public void testWasAssociatedWith() {
        WasAssociatedWith waw = new WasAssociatedWith(new QName("http://www.ll.mit.edu/prov/test", "waw1"),
                                                      new QName("http://www.ll.mit.edu/prov/test", "activity1"),
                                                      new QName("http://www.ll.mit.edu/prov/test", "agent1"));
        Graph graph = new Graph();
        graph.add(waw);
        Serializer ser = new ProvJsonSerializer();

        String str = ser.serialize(waw);
        //System.out.println(str);

        Deserializer deser = new ProvJsonDeserializer();
        Graph graph2 = null;
        try {
            graph2 = deser.deserialize(str);
        } catch (DeserializeException e) {
            assert(false);
        }
        //System.out.println(graph2);

        assert(identicalGraphs(graph, graph2));
    }

    public void testWasAttributedTo() {
        WasAttributedTo wat = new WasAttributedTo(new QName("http://www.ll.mit.edu/prov/test", "wat1"),
                                                  new QName("http://www.ll.mit.edu/prov/test", "entity1"),
                                                  new QName("http://www.ll.mit.edu/prov/test", "agent1"));
        Graph graph = new Graph();
        graph.add(wat);
        Serializer ser = new ProvJsonSerializer();

        String str = ser.serialize(wat);
        //System.out.println(str);

        Deserializer deser = new ProvJsonDeserializer();
        Graph graph2 = null;
        try {
            graph2 = deser.deserialize(str);
        } catch (DeserializeException e) {
            assert(false);
        }
        //System.out.println(graph2);

        assert(identicalGraphs(graph, graph2));
    }

    public void testWasDerivedFrom() {
        WasDerivedFrom wdf = new WasDerivedFrom(new QName("http://www.ll.mit.edu/prov/test", "wdf1"),
                                                new QName("http://www.ll.mit.edu/prov/test", "entity1"),
                                                new QName("http://www.ll.mit.edu/prov/test", "entity2"));
        Graph graph = new Graph();
        graph.add(wdf);
        Serializer ser = new ProvJsonSerializer();

        String str = ser.serialize(wdf);
        //System.out.println(str);

        Deserializer deser = new ProvJsonDeserializer();
        Graph graph2 = null;
        try {
            graph2 = deser.deserialize(str);
        } catch (DeserializeException e) {
            assert(false);
        }
        //System.out.println(graph2);

        assert(identicalGraphs(graph, graph2));
    }

    public void testWasInformedBy() {
        WasInformedBy wib = new WasInformedBy(new QName("http://www.ll.mit.edu/prov/test", "wib1"),
                                              new QName("http://www.ll.mit.edu/prov/test", "activity1"),
                                              new QName("http://www.ll.mit.edu/prov/test", "activity2"));
        Graph graph = new Graph();
        graph.add(wib);
        Serializer ser = new ProvJsonSerializer();

        String str = ser.serialize(wib);
        //System.out.println(str);

        Deserializer deser = new ProvJsonDeserializer();
        Graph graph2 = null;
        try {
            graph2 = deser.deserialize(str);
        } catch (DeserializeException e) {
            assert(false);
        }
        //System.out.println(graph2);

        assert(identicalGraphs(graph, graph2));
    }

    public void testWasGeneratedBy() {
        WasGeneratedBy wgb = new WasGeneratedBy(new QName("http://www.ll.mit.edu/prov/test", "wgb1"),
                                                new QName("http://www.ll.mit.edu/prov/test", "entity1"),
                                                new QName("http://www.ll.mit.edu/prov/test", "activity1"));
        Graph graph = new Graph();
        graph.add(wgb);
        Serializer ser = new ProvJsonSerializer();

        String str = ser.serialize(wgb);
        //System.out.println(str);

        Deserializer deser = new ProvJsonDeserializer();
        Graph graph2 = null;
        try {
            graph2 = deser.deserialize(str);
        } catch (DeserializeException e) {
            assert(false);
        }
        //System.out.println(graph2);

        assert(identicalGraphs(graph, graph2));
    }


    public void testGraph() {
        Graph graph = new Graph();

        Vector<Activity> activities = new Vector<>();
        for (int i=0;i<10;i++) {
            Activity activity = new Activity(new QName("http://www.ll.mit.edu/prov/test",
                                                       String.format("activity%d", (i+1))));
            addAttributes(activity);
            activities.add(activity);
            
        }
        Vector<Agent> agents = new Vector<>();
        for (int i=0;i<10;i++) {
            Agent agent = new Agent(new QName("http://www.ll.mit.edu/prov/test",
                                              String.format("agent%d", (i+1))));
            addAttributes(agent);
            agents.add(agent);
            graph.add(agent);
        }
        Vector<Entity> entities = new Vector<>();
        for (int i=0;i<10;i++) {
            Entity entity = new Entity(new QName("http://www.ll.mit.edu/prov/test",
                                                 String.format("entity%d", (i+1))));
            addAttributes(entity);
            entities.add(entity);
            graph.add(entity);
        }

        for (int i=0;i<5;i++) {
            ActedOnBehalfOf behalf =
                new ActedOnBehalfOf(new QName("http://www.ll.mit.edu/prov/test", String.format("behalf%d", (i+1))),
                                    agents.elementAt(random.nextInt(agents.size())).getId(),
                                    agents.elementAt(random.nextInt(agents.size())).getId());
            addAttributes(behalf);
            graph.add(behalf);
        }
        for (int i=0;i<5;i++) {
            Used used = new Used(new QName("http://www.ll.mit.edu/prov/test", String.format("used%d", (i+1))),
                                 activities.elementAt(random.nextInt(activities.size())).getId(),
                                 entities.elementAt(random.nextInt(entities.size())).getId());
            addAttributes(used);
            graph.add(used);
        }
        for (int i=0;i<5;i++) {
            WasAssociatedWith waw =
                new WasAssociatedWith(new QName("http://www.ll.mit.edu/prov/test", String.format("waw%d", (i+1))),
                                      activities.elementAt(random.nextInt(activities.size())).getId(),
                                      agents.elementAt(random.nextInt(agents.size())).getId());
            addAttributes(waw);
            graph.add(waw);
        }
        for (int i=0;i<5;i++) {
            WasAttributedTo wat =
                new WasAttributedTo(new QName("http://www.ll.mit.edu/prov/test", String.format("wat%d", (i+1))),
                                    entities.elementAt(random.nextInt(entities.size())).getId(),
                                    agents.elementAt(random.nextInt(agents.size())).getId());
            addAttributes(wat);
            graph.add(wat);
        }
        for (int i=0;i<5;i++) {
            WasDerivedFrom wdf =
                new WasDerivedFrom(new QName("http://www.ll.mit.edu/prov/test", String.format("wdf%d", (i+1))),
                                   entities.elementAt(random.nextInt(entities.size())).getId(),
                                   entities.elementAt(random.nextInt(entities.size())).getId());
            addAttributes(wdf);
            graph.add(wdf);
        }
        for (int i=0;i<5;i++) {
            WasInformedBy wib =
                new WasInformedBy(new QName("http://www.ll.mit.edu/prov/test", String.format("wib%d", (i+1))),
                                  activities.elementAt(random.nextInt(activities.size())).getId(),
                                  activities.elementAt(random.nextInt(activities.size())).getId());
            addAttributes(wib);
            graph.add(wib);
        }
        for (int i=0;i<5;i++) {
            WasGeneratedBy wgb =
                new WasGeneratedBy(new QName("http://www.ll.mit.edu/prov/test", String.format("wgb%d", (i+1))),
                                   entities.elementAt(random.nextInt(entities.size())).getId(),
                                   activities.elementAt(random.nextInt(activities.size())).getId());
            addAttributes(wgb);
            graph.add(wgb);
        }

        Serializer ser = new ProvJsonSerializer();
        String str = ser.serialize(graph);
        //System.out.println(str);

        Deserializer deser = new ProvJsonDeserializer();
        Graph graph2 = null;
        try {
            graph2 = deser.deserialize(str);
        } catch (DeserializeException e) {
            assert(false);
        }
        //System.out.println(graph2);

        assert(identicalGraphs(graph, graph2));
    }

    protected void addAttributes(Attributes attrs) {
        for (int i=0; i<random.nextInt(10); i++) {
            attrs.setAttribute(new QName("http://www.ll.mit.edu/prov/test", String.format("attr%d", (i+1))),
                               new Integer(random.nextInt()));
        }
    }


    protected boolean identicalGraphs(Graph graph1, Graph graph2) {
        if (graph1.vertices.size() != graph2.vertices.size()) {
            return false;
        }
        for (QName id : graph1.vertices.keySet()) {
            if (!graph2.vertices.containsKey(id)) {
                return false;
            }
            if (!equalVertices(graph1.vertices.get(id), graph2.vertices.get(id))) {
                return false;
            }
        }

        if (graph1.edges.size() != graph2.edges.size()) {
            return false;
        }
        for (QName id : graph1.edges.keySet()) {
            if (!graph2.edges.containsKey(id)) {
                return false;
            }
            if (!equalEdges(graph1.edges.get(id), graph2.edges.get(id))) {
                return false;
            }
        }

        // assume forward and backward are correct
        if (graph1.forward.size() != graph2.forward.size()) {
            return false;
        }
        if (graph1.backward.size() != graph2.backward.size()) {
            return false;
        }

        return true;
    }

    protected boolean equalVertices(Vertex vertex1, Vertex vertex2) {
        if (!vertex1.getId().equals(vertex2.getId())) {
            return false;
        }
        return equalAttributes(vertex1, vertex2);
    }

    protected boolean equalEdges(Edge edge1, Edge edge2) {
        if (!edge1.getId().equals(edge2.getId())) {
            return false;
        }
        if (!edge1.getSource().equals(edge2.getSource())) {
            return false;
        }
        if (!edge1.getDestination().equals(edge2.getDestination())) {
            return false;
        }
        return equalAttributes(edge1, edge2);
    }

    protected boolean equalAttributes(Attributes attrs1, Attributes attrs2) {
        if (attrs1.attrs.size() != attrs2.attrs.size()) {
            return false;
        }
        for (QName id : attrs1.attrs.keySet()) {
            if (!attrs2.attrs.containsKey(id)) {
                return false;
            }
            if (!attrs1.attrs.get(id).equals(attrs2.attrs.get(id))) {
                return false;
            }
        }
        return true;
    }

}
