This set of plugins works with [Curator](https://llcad-github.llan.ll.mit.edu/Cyber-Provenance/Curator) to capture provenance from the ActiveMQ message bus and converts the message flows into provenance.

# Installation instructions
There are several components that need installed to capture provenance for ActiveMQ

1. ActiveMQ
2. Logstash
3. MariaDB

Below, we will outline the changes needed to enable support for provenance.

## ActiveMQ

We rely on a mostly standard installation of ActiveMQ, with several plugins enabled. These plugins are part of the standard ActiveMQ installation.

The three plugins that need enabled are the `loggingBrokerPlugin`, the `timeStampingBrokerPlugin`, and the `traceBrokerPathPlugin`. The `loggingBrokerPlugin` logs events related to consumers, producers, and the messages they send and receive. The `timeStampingBrokerPlugin` ensures a consistent timestamp for log events, instead of allowing clients to specify timestamps. Finally, the `traceBrokerPathPlugin` adds additional metadata to the messages about the path of a message through a network of brokers.

To enable the plugins, simply add the following XML code to the ActiveMQ configuration file located at `<activemq-root-directory>/conf/activemq.xml`. The `<plugins>...</plugins>` block below is added to the `<broker>..</broker>` block for each ActiveMQ broker.

```xml
<plugins>
  <loggingBrokerPlugin logAll="false" logConsumerEvents="true" logProducerEvents="true"/>
  <timeStampingBrokerPlugin zeroExpirationOverride="1000" ttlCeiling="60000" futureOnly="true"/>
  <traceBrokerPathPlugin/>
</plugins>
```

This will cause ActiveMQ to log the producer- and consumer-related events to the configured log. By default, this log is stored in `<activemq-root-directory>/data/activemq.log`.

Next, filebeat must be installed and configured to send the ActiveMQ log to the Logstash server. Installation instructions for filebeat can be found [here](https://www.elastic.co/products/beats/filebeat). Using the standard installation for filebeat, the only configuration that changes is what file is monitored and a set of filters on that file. Below is the example configuration for filebeat to send ActiveMQ logs to the logstash server.

```yaml
filebeat.prospectors:
- input_type: log
  paths:
    - /var/log/activemq/activemq.log
output.logstash:
  hosts: ["logstash:5044"]
output.console:
  pretty: true
processors:
  - drop_event:
      when:
        and:
          - not:
              contains:
                message: " Adding Consumer: ConsumerInfo "
          - not:
              contains:
                message: " Adding Producer: ProducerInfo "
          - not:
              contains:
                message: " Sending message: "
          - not:
              contains:
                message: " Acknowledging message for client ID: " 
```

In the above configuration, the path of the log file will need updated. In the example above it is `/var/log/activemq/activemq.log`. The hostname/IP address of the logstash server also needs updated. The example configuration above points to a host named `logstash` on port 5044. This configuration will also output the content of the log that is being sent to STDOUT.

## Logstash

Logstash is responsible for ingesting the ActiveMQ log messages and translating them (using Curator) into PROV messages and storing them in the database.

The logstash configuration is more complex than the other sub-systems due to the need to integrate the Curator libraries. Curator is implemented in Java while Logstash is implemented using JRuby.

### Configuration directory

The main configuration directory contains two files that we need to edit/create. The configuration directory is `<logstash-root>/config`. The two files we need are a `jvm.options` file to control the configuration of the JVM and logstash pipeline.

It is necessary to set the CLASSPATH environment variable to get the CLASSPATH working for JRuby. The variable value should be (with the paths updated accordingly):

```
CLASSPATH=/opt/jars/accumulo-core-1.8.0.jar:/opt/jars/accumulo-fate-1.8.0.jar:/opt/jars/commons-collections-3.2.2.jar:/opt/jars/commons-configuration-1.6.jar:/opt/jars/commons-lang-2.4.jar:/opt/jars/commons-logging-1.1.1.jar:/opt/jars/core-2.0-SNAPSHOT.jar:/opt/jars/db-accumulo-2.0-SNAPSHOT.jar:/opt/jars/db-sql-2.0-SNAPSHOT.jar:/opt/jars/deserialize-w3c-2.0-SNAPSHOT.jar:/opt/jars/guava-14.0.1.jar:/opt/jars/hadoop-common-2.6.4.jar:/opt/jars/htrace-core-3.1.0-incubating.jar:/opt/jars/javax.json-1.0.4.jar:/opt/jars/javax.json-api-1.0.jar:/opt/jars/libthrift-0.9.3.jar:/opt/jars/log4j-1.2.17.jar:/opt/jars/logging-log4j-2.0-SNAPSHOT.jar:/opt/jars/mysql-connector-java-5.1.38.jar:/opt/jars/serialize-w3c-2.0-SNAPSHOT.jar:/opt/jars/slf4j-api-1.7.22.jar:/opt/jars/zookeeper-3.4.6.jar
```

To do this on a recent Ubuntu system running `systemd`, you can modify the file `/etc/defaults/logstash` in the following way:
```
echo "CLASSPATH=/opt/jars/accumulo-core-1.8.0.jar:/opt/jars/accumulo-fate-1.8.0.jar:/opt/jars/commons-collections-3.2.2.jar:/opt/jars/commons-configuration-1.6.jar:/opt/jars/commons-lang-2.4.jar:/opt/jars/commons-logging-1.1.1.jar:/opt/jars/core-2.0-SNAPSHOT.jar:/opt/jars/db-accumulo-2.0-SNAPSHOT.jar:/opt/jars/db-sql-2.0-SNAPSHOT.jar:/opt/jars/deserialize-w3c-2.0-SNAPSHOT.jar:/opt/jars/guava-14.0.1.jar:/opt/jars/hadoop-common-2.6.4.jar:/opt/jars/htrace-core-3.1.0-incubating.jar:/opt/jars/javax.json-1.0.4.jar:/opt/jars/javax.json-api-1.0.jar:/opt/jars/libthrift-0.9.3.jar:/opt/jars/log4j-1.2.17.jar:/opt/jars/logging-log4j-2.0-SNAPSHOT.jar:/opt/jars/mysql-connector-java-5.1.38.jar:/opt/jars/serialize-w3c-2.0-SNAPSHOT.jar:/opt/jars/slf4j-api-1.7.22.jar:/opt/jars/zookeeper-3.4.6.jar" | sudo tee -a /etc/default/logstash
```

### Pipeline Directory

Next, we need to configure the pipeline to process out ActiveMQ logs. The file is called `pipeline.yml` and contains the following pipeline configuration. The file itself is located at `<logstash-root>/pipeline/pipeline.yml`.

```
input {
    beats {
        host => "0.0.0.0"
        port => 5044
    }
}

filter {
    activemq_prov {
    }
}

output {
    curator_sql {
        driver => "com.mysql.jdbc.Driver"
        url => "jdbc:mysql://mariadb:3306/provenance"
        username => "prov"
        password => "5P@d1N6"
    }
}
```

The pipeline above enables the filebeat input plugin on the standard filebeat port, our ActiveMQ2PROV filter plugin, and the Curator SQL output plugin.

### Plugin Installation

#### Gem Installation
On you local build server, check out/download the source form [here](https://llcad-github.llan.ll.mit.edu/Cyber-Provenance/logstash-curator-plugins). For each of the needed plugins (e.g. `logstash-filter-activemq_prov` and `logstash-output-curator_sql`), you need to build the gem packages. The first block below builds the ActiveMQ to PROV filter:

```
cd logstash-curator-plugins/logstash-filter-activemq_prov
gem build logstash-filter-activemq_prov.gemspec
```

This results in the file `logstash-filter-activemq_prov-0.1.0.gem` being created. Copy this to your logstash server and run the following command:

```
/usr/share/logstash/bin/logstash-plugin install <path-to-logstash-filter-activemq_prov-0.1.0.gem>
```

Repeat the process with the `logstash-output-curator_sql` plugin using the following example commands:

```
cd logstash-curator-plugins/logstash-output-curator_sql
gem build logstash-output-curator_sql.gemspec
```

This results in the file `logstash-output_curator_sql-0.1.0.gem` being created. Copy this to your logstash server and run the following command:

```
/usr/share/logstash/bin/logstash-plugin install <path-to-logstash-output_curator_sql-0.1.0.gem>
```

#### Source Installation (not recommended unless you are developing the plugins)

The Curator Logstash plugin source can be found [here](https://llcad-github.llan.ll.mit.edu/Cyber-Provenance/logstash-curator-plugins). Download the source and place the directory `logstash-curator-plugins` somewhere on the logstash server. In the examples, we have placed them at `/opt/logstash-plugins`. Logstash then needs updated to know where to find these new plugins. The most direct way to accomplish this is to edit the `Gemfile` that is part of the standard Logstash installation and append the following lines:

```
gem "logstash-output-curator_sql", :path => "/opt/logstash-plugins/logstash-curator-plugins/logstash-output-curator_sql"
gem "logstash-output-curator_accumulo", :path => "/opt/logstash-plugins/logstash-curator-plugins/logstash-output-curator_accumulo"
gem "logstash-filter-activemq2prov", :path => "/opt/logstash-plugins/logstash-curator-plugins/logstash-filter-activemq2prov"
```

Ensure that the paths are correct, and restart Logstash. It should find the plugins without issue.

### Jar Files

Finally, the Curator libraries need built and made accessible to Logstash. This also requires several other JAR files to support things like the MariaDB interface and processing the provenance data.

The following is a list of JAR files that need placed somewhere on the Logstash server. The examples here assume they are located in the directory `/opt/jars/`.

#### Standard JAR files
The following JAR files can be found on Maven Central.

* accumulo-core-1.8.0.jar
* accumulo-fate-1.8.0.jar
* commons-collections-3.2.2.jar
* commons-configuration-1.6.jar
* commons-lang-2.4.jar
* commons-logging-1.1.1.jar
* guava-14.0.1.jar
* hadoop-common-2.6.4.jar
* htrace-core-3.1.0-incubating.jar
* javax.json-1.0.4.jar
* javax.json-api-1.0.jar
* libthrift-0.9.3.jar
* log4j-1.2.17.jar
* mysql-connector-java-5.1.38.jar
* slf4j-api-1.7.22.jar
* zookeeper-3.4.6.jar

#### Curator JAR files
The following JAR files are the Curator JAR files. In order to build Curator, see [here](https://raw.llcad-github.llan.ll.mit.edu/Cyber-Provenance/Curator/master/README.md) or the `README.md` file in the checked out repository for build instructions. Once built and installed, the JAR files can be found under `~/.m2/repository/edu/mit/ll/prov/curator/<module-name>/2.0-SNAPSHOT/` where `<module-name>` is one of `core`, `db-sql`, `deserialize-w3c`, `logging-log4j`, or `serialize-w3c`.

* core-2.0-SNAPSHOT.jar
* db-sql-2.0-SNAPSHOT.jar
* deserialize-w3c-2.0-SNAPSHOT.jar
* logging-log4j-2.0-SNAPSHOT.jar
* serialize-w3c-2.0-SNAPSHOT.jar

## MariaDB

The provenance data is stored in MariaDB. The database is a standard installation, with the following setup done after installation to create the users that Curator expects. These values can be changed and the appropriate configuration changes done in Logstash (see above for how to configure Logstash). The shell commands below create a database named `provenance`, a user named `prov`, and the appropriate permissions to allow `prov` to access `provenance`. In the commands below, we use the database root account with password `pr0venanc3` to execute these commands. Those will need to be updated based on the configuration of the database.

```bash
mysql --user=root --password=pr0venanc3 -e "create database provenance"
mysql --user=root --password=pr0venanc3 -e "create user 'prov'@'localhost' identified by '5P@d1N6'"
mysql --user=root --password=pr0venanc3 -e "grant all privileges on provenance.* to 'prov'@'localhost'"
mysql --user=root --password=pr0venanc3 -e "create user 'prov'@'%' identified by '5P@d1N6'"
mysql --user=root --password=pr0venanc3 -e "grant all privileges on provenance.* to 'prov'@'%'"
```

# Running

Once things are installed and configured, simply start all three services and allow applications to use the bus. The suite of plugins will capture the relevant log information and convert it to PROV graphs.

# Other Notes
In the `testing` directory are some files that were used to test the script.
- `testing/activemq.log`: Test ActiveMQ log that allowed for repeatability of tests. This was fed into logstash using filebeat.
- `testing/activemq.log-missing-connects`: This log removes some of the initial lines from the log to simulate what happens if logstash does not recieve the initial connection messages.
- `testing/activemq.log-missing-connects-and-sends`: Similar to the other truncated log, but this one also removes some of the send message log entries to test another edge case where we see the message acknowledgement but have not seen the sends.
- `testing/logstash.conf`: Configuration of logstash for local testing/development.
- `testing/filebeat.yml`: Configuration of filebeat for local testing/development.
- `testing/get-jars.sh`: This grabs the Curator jars from Maven and places them in a directory. This was used to update the Curator JAR files as changes were made.
- `testing/start-mariadb.sh`: I used Docker to setup a SQL server (MariaDB) for local development. This relies on the script `testing/000-mariadb.sh` to setup the user and database for Curator. Instructions on how this works can be found on the [MariaDB Docker Hub page](https://hub.docker.com/_/mariadb/) under the heading *Initializing a fresh instance*.
- `testing/connect-mysql.sh`: I installed a MySQL client from Homebrew on my local system to connect to the MariaDB Docker container. This script just automated that process.
- `testing/reset-mariadb.sh`: Instead of tearing down the MariaDB container each time, you can clear out the tables using this script. Saves a bit of time during iterations.
- `testing/start-logstash.sh`: Once all the pieces are in place, this script will start logstash. The SQL server must be started first.
- `testing/start-filebeat.sh`: Once the SQL server and logstash are both started, this script will read the file `testing/activemq.log` and send it to logstash.