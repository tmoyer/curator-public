# encoding: utf-8
require 'logstash/filters/base'
require 'logstash/namespace'
require 'java'
require 'securerandom'
require 'logger'

# For all of the PROV-JSON "stuff"
java_import 'edu.mit.ll.prov.curator.model.Agent'
java_import 'edu.mit.ll.prov.curator.model.Activity'
java_import 'edu.mit.ll.prov.curator.model.Entity'
java_import 'edu.mit.ll.prov.curator.model.Used'
java_import 'edu.mit.ll.prov.curator.model.WasAttributedTo'
java_import 'edu.mit.ll.prov.curator.model.WasGeneratedBy'
java_import 'edu.mit.ll.prov.curator.model.WasAssociatedWith'
java_import 'edu.mit.ll.prov.curator.model.Graph'
java_import 'edu.mit.ll.prov.curator.serialize.ProvJsonSerializer'

class LogStash::Filters::ActivemqProv < LogStash::Filters::Base

  config_name "activemq_prov"
  
  PROV_NAMESPACE = "http://www.w3.org/ns/prov#"
  APP_NAMESPACE = "http://www.ll.mit.edu/provenance/application"

  ID = ['clientId', 'messageId']

  public
  def register
    @ser = ProvJsonSerializer.new()
    @messagesDestinationMapping = Hash.new
    @logger = Logger.new(STDOUT)
    @logger.level = Logger::INFO
  end # def register

  public
  def filter(event)
    incomingMessage = event.to_hash["message"]
    processed = false

    if incomingMessage.include? " Adding Consumer: ConsumerInfo "
      @logger.debug("Processing add consumer")
      outgoingProv = processAddConsumer(incomingMessage)
      if outgoingProv
        event.set("message", outgoingProv)
        processed = true
      end
    end

    if incomingMessage.include? " Adding Producer: ProducerInfo "
      @logger.debug("Processing add producer")
      event.set("message", processAddProducer(incomingMessage))
      processed = true
    end

    if incomingMessage.include? " Sending message: "
      @logger.debug("Processing send message")
      event.set("message", processSendingMessage(incomingMessage))
      processed = true
    end

    if incomingMessage.include? " Acknowledging message for client ID: "
      @logger.debug("Processing ack message")
      event.set("message", processAckMessage(incomingMessage))
      processed = true
    end

    # filter_matched should go in the last line of our successful code
    if processed
      filter_matched(event)
    else
      event.cancel
    end
  end # def filter

  def processAddConsumer(message)
    messageHash = Hash.new
    messageParts = message.split('|')[2].gsub(/^ Adding Consumer: ConsumerInfo /, '').delete('{}').split(', ').each{|x|
      partsSplit = x.split(' = ')
      messageHash[partsSplit[0]] = partsSplit[1].gsub(/:/, "-").gsub(/\//, "-")
    }

    # Extract the clientId
    clientId = messageHash['clientId'][0,35]
    if clientId != "null"
      # Extract the destination
      destination = messageHash['destination']
      @logger.debug("[#{__LINE__}] Processing destination: #{destination}")
      destinationActivity = Activity.new(aqn(destination))
      
      @logger.debug("[#{__LINE__}] Processing clientId: #{clientId}")
      # attrs is a Java Map<QName, Object>
      attrs = java.util.HashMap.new
      attrs.put(pqn("type"), "SoftwareAgent")
      consumerAgent = Agent.new(aqn(clientId), attrs)
      
      graph = Graph.new
      graph.add(destinationActivity)
      graph.add(consumerAgent)
      
      return @ser.serialize(graph)
    else
      return nil
    end
  end

  def processAddProducer(message)
    messageHash = Hash.new
    messageParts = message.split('|')[2].gsub(/^ Adding Producer: ProducerInfo /, '').delete('{}').split(', ').each{|x|
      partsSplit = x.split(' = ')
      messageHash[partsSplit[0]] = partsSplit[1].gsub(/:/, "-").gsub(/\//, "-")
    }

    # Extract the producerId
    producerId = messageHash['producerId'][0,35]
    # attrs is a Java Map<QName, Object>
    attrs = java.util.HashMap.new
    attrs.put(pqn("type"), "SoftwareAgent")
    producerAgent = Agent.new(aqn(producerId), attrs)
    # Extract the destination
    destination = messageHash['destination']
    destinationActivity = Activity.new(aqn(destination))

    graph = Graph.new
    graph.add(destinationActivity)
    graph.add(producerAgent)

    return @ser.serialize(graph)
  end

  def processSendingMessage(message)
    graph = Graph.new

    messageHash = Hash.new
    messageParts = message.split('|')[2].gsub(/^ Sending message: ActiveMQObjectMessage /, '').delete('{}').split(', ').each{|x|
      partsSplit = x.split(' = ')
      messageHash[partsSplit[0]] = partsSplit[1].gsub(/:/, "-").gsub(/\//, "-")
    }

    # Extract the messageId
    messageId = messageHash['messageId']
    #puts "[#{__LINE__} Processing messageId: #{messageId}"

    # Extract the producerId
    producerId = messageHash['producerId'][0,35]
    #puts "[#{__LINE__} Processing producerId: #{producerId}"

    # Extract the destination
    destination = messageHash['destination']
    #puts "[#{__LINE__} Processing destination: #{destination}"
    
    # attrs is a Java Map<QName, Object>
    attrs = java.util.HashMap.new
    attrs.put(pqn("type"), "SoftwareAgent")
    producerAgent = Agent.new(aqn(producerId), attrs)
    graph.add(producerAgent)

    # Extract the destination
    destination = messageHash['destination']
    destinationActivity = Activity.new(aqn(destination))
    graph.add(destinationActivity)

    # Generate a message entity
    # Attributes of message entity: producerId, destination
    attrs = java.util.HashMap.new
    attrs.put(aqn("producerId"), producerId)
    attrs.put(aqn("destination"), destination)
    messageEntity = Entity.new(aqn(messageId + "-IN"), attrs)
    graph.add(messageEntity)

    # Create map from messageId to destination so we can later create the link between the client and the destination
    @messagesDestinationMapping[messageId] = destination
    @logger.debug("Added #{messageId} => #{destination} to the map")

    # Generate a used edge (activity->entity)
    used = Used.new(id(), destinationActivity.getId(), messageEntity.getId())
    graph.add(used)

    # Generate a wasAttributedTo edge (entity->agent)
    wasAttributedTo = WasAttributedTo.new(id(), messageEntity.getId(), aqn(producerId))
    graph.add(wasAttributedTo)

    # Generate a wasAssociatedWith edge (activity->agent)
    wasAssociatedWith = WasAssociatedWith.new(id(), destinationActivity.getId(), aqn(producerId))
    graph.add(wasAssociatedWith)

    return @ser.serialize(graph)
  end

  def processAckMessage(message)
    graph = Graph.new

    messageHash = Hash.new
    messageParts = message.split('|')[2].gsub(/^ Acknowledging message for client ID: /, '').split(', ')

    messageHash = Hash.new
      messageParts.each_index{|x|
      messageHash[ID[x]] = messageParts[x].strip
    }

    # Extract the clientId (1st ID)
    clientId = messageHash['clientId'].gsub(/:/, "-").gsub(/\//, "-")[0,35]
    # attrs is a Java Map<QName, Object>
    attrs = java.util.HashMap.new
    attrs.put(pqn("type"), "SoftwareAgent")
    consumerAgent = Agent.new(aqn(clientId), attrs)
    graph.add(consumerAgent)

    # Extract the message Id (2nd ID)
    messageId = messageHash['messageId'].gsub(/:/, "-").gsub(/\//, "-")
    
    # need to determine what the destination activity is
    @logger.debug("Looking for #{messageId} in the map")
    destination = @messagesDestinationMapping[messageId]
    if destination
      destinationActivity = Activity.new(aqn(destination))
      graph.add(destinationActivity)
    end

    attrs = java.util.HashMap.new
    if destination
      attrs.put(aqn("destination"), destination)
      messageEntity = Entity.new(aqn(messageId + "-OUT"), attrs)
    else
      messageEntity = Entity.new(aqn(messageId + "-OUT"))
    end
    graph.add(messageEntity)
    
    # Generate a wasAttributedTo edge (entity->agent)
    wasAttributedTo = WasAttributedTo.new(id(), messageEntity.getId(), aqn(clientId))
    graph.add(wasAttributedTo)

    if destination
      # Generate a wasGeneratedByEdge (entity->activity)
      wasGeneratedBy = WasGeneratedBy.new(id(), messageEntity.getId(), aqn(destination))
      graph.add(wasGeneratedBy)

      # Generate a wasAssociatedWith edge (activity->agent)
      wasAssociatedWith = WasAssociatedWith.new(id(), aqn(destination), aqn(clientId))
      graph.add(wasAssociatedWith)
    end
      
    return @ser.serialize(graph)
  end

  def pqn(name)
    javax.xml.namespace.QName.new(PROV_NAMESPACE, name)
  end

  def aqn(name)
    javax.xml.namespace.QName.new(APP_NAMESPACE, name)
  end

  def id(prefix = "id")
    return aqn(prefix + "-" + SecureRandom.uuid )
  end
end # class LogStash::Filters::Activemq2prov
