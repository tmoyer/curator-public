# encoding: utf-8
require "logstash/outputs/base"
require "logstash/namespace"
require "java"

java_import "edu.mit.ll.prov.curator.db.SQL"
java_import "edu.mit.ll.prov.curator.deserialize.ProvJsonDeserializer"
java_import "edu.mit.ll.prov.curator.db.StoreException"

# An curator-sql output that does nothing.
class LogStash::Outputs::CuratorSql < LogStash::Outputs::Base
  config_name "curator_sql"

  # Access these via @<name>, e.g. @url, @driver, etc.
  config :driver, :validate => :string, :required => true
  config :url, :validate => :string, :required => true
  config :username, :validate => :string, :required => true
  config :password, :validate => :string, :required => true
  
  public
  def register
    # Instantiate an edu.mit.ll.prov.curator.db.SQL object
    @store = SQL.new(@driver, @url, @username, @password)

    # Instantiate an edu.mit.ll.prov.curator.deserialize.ProvJsonDeserializer object
    @deser = ProvJsonDeserializer.new()

  end # def register

  public
  def multi_receive(events)
    events.each do |event|
        begin
          @store.write(@deser.deserialize(event.to_hash["message"]))
        rescue StoreException => e
          
        end
    end
    return
  end # def event
end # class LogStash::Outputs::CuratorSql
