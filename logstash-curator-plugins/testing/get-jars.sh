#!/bin/bash

mkdir -p ~/Scratch/jars

cp $HOME/.m2/repository/edu/mit/ll/prov/curator/core/2.0-SNAPSHOT/core-2.0-SNAPSHOT.jar ~/Scratch/jars/
cp $HOME/.m2/repository/edu/mit/ll/prov/curator/logging-log4j/2.0-SNAPSHOT/logging-log4j-2.0-SNAPSHOT.jar ~/Scratch/jars/
cp $HOME/.m2/repository/edu/mit/ll/prov/curator/db-sql/2.0-SNAPSHOT/db-sql-2.0-SNAPSHOT.jar ~/Scratch/jars/
cp $HOME/.m2/repository/edu/mit/ll/prov/curator/deserialize-w3c/2.0-SNAPSHOT/deserialize-w3c-2.0-SNAPSHOT.jar ~/Scratch/jars/
cp $HOME/.m2/repository/edu/mit/ll/prov/curator/serialize-w3c/2.0-SNAPSHOT/serialize-w3c-2.0-SNAPSHOT.jar ~/Scratch/jars/
cp $HOME/.m2/repository/edu/mit/ll/prov/curator/db-accumulo/2.0-SNAPSHOT/db-accumulo-2.0-SNAPSHOT.jar ~/Scratch/jars/

pushd ~/Scratch
tar cf jars.tar jars/
popd