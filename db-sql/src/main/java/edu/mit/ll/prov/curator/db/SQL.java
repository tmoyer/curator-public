package edu.mit.ll.prov.curator.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;

import javax.xml.namespace.QName;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.mit.ll.prov.curator.model.*;

import edu.mit.ll.prov.curator.db.Database;

public class SQL implements Database  {

    protected SqlConfiguration config;

    protected Connection conn = null;

    private final static Logger logger = LoggerFactory.getLogger(SQL.class);
    

    public SQL(SqlConfiguration config) throws DatabaseException {
        this.config = config;
        connect();
        createTables();
    }

    public SQL(String driver, String url, String username, String password) 
        throws DatabaseException {
        
        this.config = new SqlConfiguration();
        this.config.setDriver(driver);
        this.config.setUrl(url);
        this.config.setUsername(username);
        this.config.setPassword(password);

        connect();
        createTables();
    }

    final protected void connect() throws DatabaseException {
        try {
            Class.forName(config.getDriver()).newInstance();
            if ((config.getUsername() == null) || "".equals(config.getUsername())) {
                conn = DriverManager.getConnection(config.getUrl());
            } else {
                conn = DriverManager.getConnection(config.getUrl(), config.getUsername(), config.getPassword());
            }
            conn.setAutoCommit(true);
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | SQLException e) {
            throw new DatabaseException("Error establishing connection", e);
        }
    }

    // subclass to handle the variants?
    final protected void createTables() throws DatabaseException {
        try {
            // Create tables if they do not already exist
            createTable(getCreateTable("vertex_type")
                        + "(" + getAutoIncrement("id")
                        + "type VARCHAR(128)"
                        + ")");
            createTable(getCreateTable("edge_type")
                        + "(" + getAutoIncrement("id")
                        + "type VARCHAR(128)"
                        + ")");
            createTable(getCreateTable("namespace")
                        + "(" + getAutoIncrement("id")
                        + "namespace VARCHAR(128)"
                        + ")");

            createTable(getCreateTable("vertex")
                        + "(" + getAutoIncrement("id")
                        + "type INT REFERENCES vertex_type, "
                        + "name VARCHAR(128) NOT NULL, "
                        + "namespace INT REFERENCES namespace, "
                        + "UNIQUE (name,namespace)"
                        + ")");
            createTable(getCreateTable("vertex_attrs")
                        + "(vertex INT REFERENCES vertex, "
                        + "name VARCHAR(128) NOT NULL, "
                        + "namespace INT REFERENCES namespace, "
                        + "value VARCHAR(128) NOT NULL, "
                        + "type VARCHAR(128) NOT NULL, "
                        + "type_namespace INT REFERENCES namespace, "
                        + "PRIMARY KEY (vertex,name,namespace)"
                        + ")");
            createTable(getCreateTable("edge")
                        + "(" + getAutoIncrement("id")
                        + "type INT REFERENCES edge_type, "
                        + "name VARCHAR(128) NOT NULL, "
                        + "namespace INT REFERENCES namespace, "
                        + "source INT REFERENCES vertex, "
                        + "destination INT REFERENCES vertex, "
                        + "UNIQUE (name,namespace)"
                        + ")");
            createTable(getCreateTable("edge_attrs")
                        + "(edge INT REFERENCES edge, "
                        + "name VARCHAR(128) NOT NULL, "
                        + "namespace INT REFERENCES namespace, "
                        + "value VARCHAR(128) NOT NULL, "
                        + "type VARCHAR(128) NOT NULL, "
                        + "type_namespace INT REFERENCES namespace, "
                        + "PRIMARY KEY (edge,name,namespace)"
                        + ")");
        } catch (SQLException ex) {
            throw new DatabaseException("Error Creating tables", ex);
        }

        try (java.sql.Statement stmt = conn.createStatement()) {
            stmt.addBatch("CREATE INDEX vertex_index on vertex_attrs (vertex)");
            stmt.addBatch("CREATE INDEX nv_index on vertex_attrs (name,value)"); // for demoing 
            stmt.addBatch("CREATE INDEX type_index on vertex (type)");
            stmt.addBatch("CREATE INDEX type_index on vertex_type (type)");
            stmt.addBatch("CREATE INDEX source_index on edge (source)");
            stmt.addBatch("CREATE INDEX destination_index on edge (destination)");
            stmt.addBatch("CREATE INDEX edge_index on edge_attrs (edge)");
            stmt.addBatch("CREATE INDEX namespace_index on namespace (namespace)");
            stmt.executeBatch();
        } catch (SQLException ex) {
            // any errors are typically because they already exist
            logger.debug("Duplicate index", ex);
        }
    }

    public Map<String,List<String>> getCategories() throws QueryException {
        // Build final SQL query 
        String sql = "SELECT DISTINCT vertex_attrs.value as category, va.value AS name " + 
                        "FROM vertex_attrs JOIN vertex_attrs AS va ON va.vertex=vertex_attrs.vertex " + 
                        "WHERE vertex_attrs.name='category' AND va.name='name'";
        logger.debug(sql);
        
        Map<String,List<String>> ids = new HashMap<>();
        try (Statement stmt = conn.createStatement();
             ResultSet rs = stmt.executeQuery(sql)) {
            while (rs.next()) {
                if (!ids.containsKey(rs.getString("category"))) {
                    ids.put(rs.getString("category"), new ArrayList<String>());
                }
                ids.get(rs.getString("category")).add(rs.getString("name"));
            }
        } catch (SQLException ex) {
            // just give them nothing 
            logger.debug("Can't load categories from database", ex);
        }
        logger.debug(String.format("found %d ids", ids.size()));
        return ids;
    }

    protected String getCreateTable(String tableName) {
        if (config.getDriver().contains("org.apache.derby")) {
            return "CREATE TABLE " +tableName + " ";
        }
        // PostgreSQL, MySQL
        return "CREATE TABLE IF NOT EXISTS " +tableName + " ";
    }

    protected String getAutoIncrement(String columnName) {
        if (config.getDriver().contains("org.postgresql")) {
            return columnName + " SERIAL PRIMARY KEY, ";
        } else if (config.getDriver().contains("org.apache.derby")) {
            return columnName + " INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY, ";
        }
        // MySQL
        return columnName + " INT PRIMARY KEY AUTO_INCREMENT, ";
    }

    protected void createTable(String sql) throws SQLException {
        logger.debug(sql);
        
        try (java.sql.Statement stmt = conn.createStatement()) {
            stmt.execute(sql);
        } catch (SQLException e) {
            if (config.getDriver().contains("org.apache.derby") && "X0Y32".equals(e.getSQLState())) {
                return; // exception because the table exists, so no worries
            }
            throw e;
        }
    }
    
    public void clearDatabase() throws DatabaseException {
        try (java.sql.Statement stmt = conn.createStatement()) {
            stmt.addBatch("TRUNCATE TABLE vertex");
            stmt.addBatch("TRUNCATE TABLE vertex_attrs");
            stmt.addBatch("TRUNCATE TABLE edge");
            stmt.addBatch("TRUNCATE TABLE edge_attrs");
            stmt.executeBatch();
        } catch (SQLException ex) {
            throw new DatabaseException("Error clearing tables", ex);
        }
    }
    
    public void write(Graph graph) throws StoreException {
        StoreException firstException = null;
        for (Vertex vertex : graph.vertices.values()) {
            try {
                write(vertex);
            } catch (StoreException e) {
                if (firstException == null) {
                    firstException = e;
                }
            }
        }
        for (Edge edge : graph.edges.values()) {
            try {
                write(edge);
            } catch (StoreException e) {
                if (firstException == null) {
                    firstException = e;
                }
            }
        }
        if (firstException != null) {
            throw new StoreException("writing graph failed", firstException);
        }
    }
    
    public void write(Vertex vertex) throws StoreException {
        if (conn == null) {
            throw new StoreException("no connection - cannot write vertex");
        }
        int vertexId;
        try {
            String sql = String.format("INSERT INTO vertex (type, name, namespace) VALUES (%d,'%s',%d)",
                                       getVertexType(vertex),
                                       vertex.getId().getLocalPart(),
                                       getNamespace(vertex.getId().getNamespaceURI()));
            logger.debug(sql);
            try (java.sql.Statement stmt = conn.createStatement()) {
                stmt.executeUpdate(sql, java.sql.Statement.RETURN_GENERATED_KEYS);
                try (ResultSet rs = stmt.getGeneratedKeys()) {
                    rs.next();
                    vertexId = rs.getInt(1);
                }
            }
        } catch (SQLException e) {
            throw new StoreException("Error writing vertex", e);
        }

        for (QName attrName : vertex.getAttributeNames()) {
            String attrValue = vertex.getAttributeValue(attrName).toString();
            QName valueType = vertex.getAttributeType(attrName);
            try {
                String sql = String.format("INSERT INTO vertex_attrs "
                                           + "(vertex, name, namespace, value, type, type_namespace) "
                                           + "VALUES (%d,'%s',%d,'%s','%s',%d)",
                                           vertexId,
                                           attrName.getLocalPart(), getNamespace(attrName.getNamespaceURI()),
                                           attrValue,
                                           valueType.getLocalPart(), getNamespace(valueType.getNamespaceURI()));
                logger.debug(sql);
                try (java.sql.Statement stmt = conn.createStatement()) {
                    stmt.execute(sql);
                }
            } catch (Exception e) {
                throw new StoreException("Error adding vertex attribute", e);
            }
        }
    }

    protected int getVertexType(Vertex vertex) throws SQLException, StoreException {
        return getType("vertex_type", vertex.getType());
    }

    protected int getEdgeType(Edge edge) throws SQLException, StoreException {
        return getType("edge_type", edge.getType());
    }
    
    protected int getType(String table, String typeStr) throws SQLException {
        String sql = String.format("SELECT id FROM %s WHERE type='%s'", table, typeStr);
        logger.debug(sql);
        try (java.sql.Statement stmt = conn.createStatement();
             ResultSet rs = stmt.executeQuery(sql)) {
            if (rs.next()) {
                return rs.getInt(1);
            }
        }
        return addType(table, typeStr);
    }

    protected int addType(String table, String typeStr) throws SQLException {
        try (java.sql.Statement stmt = conn.createStatement()) {
            stmt.executeUpdate(String.format("INSERT INTO %s (type) VALUES ('%s')", table, typeStr),
                               java.sql.Statement.RETURN_GENERATED_KEYS);
            try (ResultSet rs = stmt.getGeneratedKeys()) {
                if (rs.next()) {
                    return rs.getInt(1);
                } else {
                    throw new SQLException("couldn't create type " + typeStr);
                }
            }
        }
    }

    protected int getNamespace(String uri) throws SQLException {
        try (java.sql.Statement stmt = conn.createStatement();
             ResultSet rs = stmt.executeQuery(String.format("SELECT * FROM namespace WHERE namespace='%s'",uri))) {
            if (rs.next()) {
                return rs.getInt(1);
            }
        }
        return addNamespace(uri);
    }

    protected int addNamespace(String uri) throws SQLException {
        try (java.sql.Statement stmt = conn.createStatement()) {
            stmt.executeUpdate(String.format("INSERT INTO namespace (namespace) VALUES ('%s')", uri),
                               java.sql.Statement.RETURN_GENERATED_KEYS);
            try (ResultSet rs = stmt.getGeneratedKeys()) {
                if (rs.next()) {
                    return rs.getInt(1);
                } else {
                    throw new SQLException("couldn't create name space " + uri);
                }
            }
        }
    }
    
    public void write(Edge edge) throws StoreException {
        if (conn == null) {
            throw new StoreException("no connection - cannot write edge");
        }
        int edgeId;
        try {
            String sql = String.format("INSERT INTO edge (type, name, namespace, source, destination) "
                                       + "VALUES (%d,'%s',%d,%d,%d)",
                                       getEdgeType(edge),
                                       edge.getId().getLocalPart(),
                                       getNamespace(edge.getId().getNamespaceURI()),
                                       getVertexNumber(edge.getSource()),
                                       getVertexNumber(edge.getDestination()));
            logger.debug(sql);
            try (java.sql.Statement stmt = conn.createStatement()) {
                stmt.executeUpdate(sql, java.sql.Statement.RETURN_GENERATED_KEYS);
                try (ResultSet rs = stmt.getGeneratedKeys()) {
                    rs.next();
                    edgeId = rs.getInt(1);
                }
            }
        } catch (SQLException e) {
            throw new StoreException("Error adding edge", e);
        }

        for (QName attrName : edge.getAttributeNames()) {
            String attrValue = edge.getAttributeValue(attrName).toString();
            QName valueType = edge.getAttributeType(attrName);
            try {
                String sql = String.format("INSERT INTO edge_attrs "
                                           + "(edge, name, namespace, value, type, type_namespace) "
                                           + "VALUES (%d,'%s',%d,'%s','%s',%d)",
                                           edgeId,
                                           attrName.getLocalPart(), getNamespace(attrName.getNamespaceURI()),
                                           attrValue,
                                           valueType.getLocalPart(), getNamespace(valueType.getNamespaceURI()));
                logger.debug(sql);
                try (java.sql.Statement stmt = conn.createStatement()) {
                    stmt.execute(sql);
                }
            } catch (Exception e) {
                throw new StoreException("Error adding edge attribute", e);
            }
        }
    }
    
    public List<Vertex> findVertices(QName attrName, Object attrValue) throws QueryException {
        return findVertices(Collections.singletonMap(attrName, attrValue));
    }
    public List<Vertex> findVertices(QName attrName, 
                                        Object attrValue, 
                                        String vertexType, 
                                        boolean either, 
                                        Integer paginate) 
                                        throws QueryException {
        return findVertices(Collections.singletonMap(attrName, attrValue), vertexType, either, paginate);
    }
    
    public List<Vertex> findVertices(Map<QName, Object> attrs) throws QueryException {
        return findVertices(attrs, null, false, null);
    }
    public List<Vertex> findVertices(Map<QName, Object> attrs, 
                                    String vertexType, 
                                    boolean either, 
                                    Integer paginate) 
                                    throws QueryException {
        Set<Integer> vertexIds = null;
        
        try {
            // Give us everything you've got! 
            if (attrs.size() == 0) {
                vertexIds = sqlFindVertexIds(null, null, vertexType, either, paginate);
            }
        } catch (SQLException e) {
            throw new QueryException("Failed to find vertices", e);
        }
        
        // Otherwise filter by given attrs 
        Set<Map.Entry<QName, Object>> attrSet = attrs.entrySet();
        for (Map.Entry<QName, Object> attr : attrSet) {
            try {
                if (vertexIds == null) {
                    vertexIds = sqlFindVertexIds(attr.getKey(), attr.getValue(), vertexType, either, paginate);
                } else {
                    vertexIds.retainAll(sqlFindVertexIds(attr.getKey(), attr.getValue(), vertexType, either, paginate));
                }
            } catch (SQLException e) {
                throw new QueryException("Failed to find vertices", e);
            }
        }
        List<Vertex> vertices = new ArrayList<>();
        if (vertexIds == null) {
            return vertices;
        } else {
            for (Integer id : vertexIds) {
                vertices.add(getVertex(id));
            }
            logger.debug(String.format("found %d vertices", vertices.size()));
            return vertices;
        }
    }

    protected Set<Integer> sqlFindVertexIds(QName attrName, Object attrValue, 
                                            String vertexType, boolean either, 
                                            Integer paginate) 
                                            throws SQLException, QueryException {
        // Allow matching on 'name AND value' or 'name OR value'
        String eitherStr = "AND";
        if (either) {
            eitherStr = "OR";
        }
        
        if (conn == null) {
            throw new QueryException("no connection - cannot find vertices");
        }
        
        // Implement pagination 
        String paginateStr = "";
        if (paginate != null) {
            paginateStr = String.format(" LIMIT %d, 10 ", paginate);
        }
        
        // Vertex type filtering 
        String vertTypeStr = "";
        if (vertexType != null) {
            vertTypeStr = String.format(" AND vt.type='%s' ", vertexType);
        }
        
        // Implement filtering 
        String sqlFilter = "1=1";
        String attrJoin = "";
        if (attrName != null) {
            sqlFilter = String.format("(ns.namespace='%s' AND vertex_attrs.name='%s' %s vertex_attrs.value='%s')", 
                                    attrName.getNamespaceURI(), attrName.getLocalPart(), 
                                    eitherStr, attrValue);
            attrJoin = "JOIN vertex_attrs ON vertex.id=vertex_attrs.vertex " +
                       "JOIN namespace AS ns ON vertex_attrs.namespace=ns.id ";
        }
        
        // Build final SQL query 
        String sql = String.format("SELECT DISTINCT(vertex.id) FROM vertex " +
                                   "%s " +
                                   "JOIN vertex_type AS vt ON vertex.type=vt.id " +
                                   "WHERE %s %s ORDER BY vertex.id %s", attrJoin, sqlFilter, vertTypeStr, paginateStr);
        logger.debug(sql);
        Set<Integer> ids = new HashSet<>();
        try (Statement stmt = conn.createStatement();
             ResultSet rs = stmt.executeQuery(sql)) {
            while (rs.next()) {
                ids.add(rs.getInt("vertex.id"));
            }
        }
        logger.debug(String.format("found %d ids", ids.size()));
        return ids;
    }

    // this could be done with a single select/join
    public Vertex getVertex(QName vertexId) throws QueryException {
        try {
            return getVertex(vertexId, getVertexNumber(vertexId));
        } catch (SQLException e) {
            throw new QueryException("failed to get vertex", e);
        }
    }

    // this could be done with a single select/join
    public Vertex getVertex(int vertexNumber) throws QueryException {
        try {
            return getVertex(getVertexId(vertexNumber), vertexNumber);
        } catch (SQLException e) {
            throw new QueryException("failed to get vertex", e);
        }
    }
    
    protected Vertex getVertex(QName vertexId, int vertexNumber) throws QueryException {
        if (conn == null) {
            throw new QueryException("no connection - cannot get vertex");
        }
        String sql = String.format("SELECT vertex_type.type FROM vertex_type " +
                                   "JOIN vertex ON vertex.type=vertex_type.id " +
                                   "WHERE vertex.id=%d",
                                   vertexNumber);
        Vertex vertex = null;
        try {
            try (Statement stmt = conn.createStatement();
                 ResultSet rs = stmt.executeQuery(sql)) {
                if (rs.next()) {
                    String vertexType = rs.getString("type");
                    if ("activity".equals(vertexType)) {
                        vertex = new Activity(vertexId);
                    } else if ("agent".equals(vertexType)) {
                        vertex = new Agent(vertexId);
                    } else if ("entity".equals(vertexType)) {
                        vertex = new Entity(vertexId);
                    } else {
                        throw new QueryException(String.format("unknown vertex type: %s", vertexType));
                    }
                    getVertexAttributes(vertexNumber, vertex);
                }
            }
        } catch (SQLException e) {
            throw new QueryException(String.format("unknown vertex %d", vertexNumber), e);
        }
        return vertex;
    }

    protected void getVertexAttributes(int vertexNumber, Vertex vertex) throws SQLException {
        String sql = String.format("SELECT name, ns1.namespace, value, type, ns2.namespace FROM vertex_attrs " +
                                   "JOIN namespace AS ns1 ON vertex_attrs.namespace=ns1.id " +
                                   "JOIN namespace AS ns2 ON vertex_attrs.type_namespace=ns2.id " +
                                   "WHERE vertex=%d", vertexNumber);
        try (Statement stmt = conn.createStatement();
             ResultSet rs = stmt.executeQuery(sql)) {
            while (rs.next()) {
                // H2 and Derby have the column labels as the original column names (e.g. namespace, not ns1.namespace)
                // so use column indices
                QName nameQn = new QName(rs.getString(2), rs.getString("name"));
                QName typeQn = new QName(rs.getString(5), rs.getString("type"));
                vertex.setAttribute(nameQn, rs.getString("value"), typeQn);
            }
        }
    }

    
    public Edge getEdge(QName edgeId) throws QueryException {
        if (conn == null) {
            throw new QueryException("no connection - cannot get vertex");
        }
        int edgeNumber = -1;
        try {
            edgeNumber = getEdgeNumber(edgeId);
        } catch (SQLException e) {
            throw new QueryException(e);
        }
        String sql = String.format("SELECT edge_type.type, edge.source, edge.destination FROM edge " +
                                   "JOIN edge_type ON edge.type=edge_type.id " +
                                   "WHERE edge.id=%d",
                                   edgeNumber);
        Edge edge = null;
        try {
            try (Statement stmt = conn.createStatement();
                 ResultSet rs = stmt.executeQuery(sql)) {
                if (rs.next()) {
                    String edgeType = rs.getString("type");
                    QName source = getVertexId(rs.getInt("edge.source"));
                    QName destination = getVertexId(rs.getInt("edge.destination"));
                    edge = edge(edgeType, edgeId, source, destination);
                    getEdgeAttributes(edgeNumber, edge);
                }
            }
        } catch (SQLException e) {
            throw new QueryException(String.format("unknown edge %d", edgeNumber), e);
        }
        return edge;
    }

    protected Edge edge(String edgeType, QName id, QName source, QName destination) 
        throws QueryException {
        
        switch (edgeType) {
        case "actedOnBehalfOf":
            return new ActedOnBehalfOf(id, source, destination);
        case "used":
            return new Used(id, source, destination);
        case "wasAssociatedWith":
            return new WasAssociatedWith(id, source, destination);
        case "wasAttributedTo":
            return new WasAttributedTo(id, source, destination);
        case "wasDerivedFrom":
            return new WasDerivedFrom(id, source, destination);
        case "wasGeneratedBy":
            return new WasGeneratedBy(id, source, destination);
        case "wasInformedBy":
            return new WasInformedBy(id, source, destination);
        default:
            throw new QueryException(String.format("unknown edge type: %s", edgeType));
        }
    }

    public Graph ancestors(QName vertexId) throws QueryException {
        Graph graph = new Graph();
        Queue<Vertex> vertices = new ArrayDeque<>();
        vertices.add(getVertex(vertexId));

        while (vertices.size() > 0) {
            Vertex vertex = vertices.remove();
            graph.add(vertex);
            List<Edge> edges = outgoing(vertex.getId());
            for(Edge edge : edges) {
                Vertex v = getVertex(edge.getDestination());
                if (graph.vertices.containsKey(v.getId())) {
                    logger.warn(String.format("found a cycle - vertex %s already in graph", v.getId()));
                } else {
                    vertices.add(v);
                    graph.add(v);
                }
                graph.add(edge);
            }
        }
        return graph;
    }

    protected List<Edge> outgoing(QName sourceId) throws QueryException {
        if (conn == null) {
            throw new QueryException("no connection - cannot find edges");
        }
        List<Edge> edges = new ArrayList<>();
        try {
            int vertexId = getVertexNumber(sourceId);
            String sql = String.format("SELECT edge.id, edge_type.type, edge.name, ns1.namespace, destination, vertex.name, ns2.namespace " +
                                       "FROM edge " +
                                       "JOIN edge_type ON edge.type=edge_type.id " +
                                       "JOIN namespace AS ns1 ON edge.namespace=ns1.id " +
                                       "JOIN vertex ON edge.destination=vertex.id " +
                                       "JOIN namespace AS ns2 ON vertex.namespace=ns2.id " +
                                       "WHERE source=%d", vertexId);
            try (Statement stmt = conn.createStatement();
                 ResultSet rs = stmt.executeQuery(sql)) {
                while (rs.next()) {
                    // H2 and Derby have the column labels as the original column names
                    // (e.g. namespace, not ns1.namespace), so use column indices
                    String edgeType = rs.getString("edge_type.type");
                    QName edgeId = new QName(rs.getString(4), rs.getString("edge.name"));
                    QName destId = new QName(rs.getString(7), rs.getString("vertex.name"));
                    Edge edge = edge(edgeType, edgeId, sourceId, destId);
                    getEdgeAttributes(rs.getInt("edge.id"), edge);
                    edges.add(edge);
                }
            }
        } catch (SQLException e) {
            throw new QueryException(e);
        }
        return edges;
    }

    protected int getVertexNumber(QName vertexId) throws SQLException {
        String sql = String.format("SELECT vertex.id FROM vertex JOIN namespace ON vertex.namespace=namespace.id " +
                                   "WHERE vertex.name='%s' AND namespace.namespace='%s'",
                                   vertexId.getLocalPart(), vertexId.getNamespaceURI());
        try (Statement stmt = conn.createStatement();
             ResultSet rs = stmt.executeQuery(sql)) {
            if (rs.next()) {
                return rs.getInt(1);
            }
        }
        throw new SQLException(String.format("didn't find vertex %s in %s",
                                             vertexId.getLocalPart(), vertexId.getNamespaceURI()));
    }

    protected QName getVertexId(int vertexNumber) throws SQLException {
        String sql = String.format("SELECT name, namespace.namespace FROM vertex " +
                                   "JOIN namespace ON vertex.namespace=namespace.id " +
                                   "WHERE vertex.id=%d",
                                   vertexNumber);
        try (Statement stmt = conn.createStatement();
             ResultSet rs = stmt.executeQuery(sql)) {
            if (rs.next()) {
                return new QName(rs.getString("namespace.namespace"), rs.getString("name"));
            }
        }
        throw new SQLException(String.format("didn't find vertex %d", vertexNumber));
    }

    protected int getEdgeNumber(QName edgeId) throws SQLException {
        String sql = String.format("SELECT edge.id FROM edge JOIN namespace ON edge.namespace=namespace.id " +
                                   "WHERE edge.name='%s' AND namespace.namespace='%s'",
                                   edgeId.getLocalPart(), edgeId.getNamespaceURI());
        try (Statement stmt = conn.createStatement();
             ResultSet rs = stmt.executeQuery(sql)) {
            if (rs.next()) {
                return rs.getInt(1);
            }
        }
        throw new SQLException(String.format("didn't find edge %s in %s",
                                             edgeId.getLocalPart(), edgeId.getNamespaceURI()));
    }

    protected void getEdgeAttributes(int edgeNumber, Edge edge) throws SQLException {
        String sql = String.format("SELECT name, ns1.namespace, value, type, ns2.namespace FROM edge_attrs " +
                                   "JOIN namespace AS ns1 ON edge_attrs.namespace=ns1.id " +
                                   "JOIN namespace AS ns2 ON edge_attrs.type_namespace=ns2.id " +
                                   "WHERE edge=%d", edgeNumber);
        try (Statement stmt = conn.createStatement();
             ResultSet rs = stmt.executeQuery(sql)) {
            while (rs.next()) {
                // H2 and Derby have the column labels as the original column names (e.g. namespace, not ns1.namespace)
                // so use column indices
                QName nameQn = new QName(rs.getString(2), rs.getString(1));
                QName typeQn = new QName(rs.getString(5), rs.getString(4));
                edge.setAttribute(nameQn, rs.getString(3), typeQn);
            }
        }
    }

    
    public Graph descendants(QName vertexId) throws QueryException {
        Graph graph = new Graph();
        Queue<Vertex> vertices = new ArrayDeque<>();
        vertices.add(getVertex(vertexId));

        while (vertices.size() > 0) {
            Vertex vertex = vertices.remove();
            graph.add(vertex);
            List<Edge> edges = incoming(vertex.getId());
            for(Edge edge : edges) {
                Vertex v = getVertex(edge.getSource());
                if (graph.vertices.containsKey(v.getId())) {
                    logger.warn(String.format("found a cycle - vertex %s already in graph", v.getId()));
                } else {
                    vertices.add(v);
                    graph.add(v);
                }
                graph.add(edge);
            }
        }
        return graph;
    }

    protected List<Edge> incoming(QName destId) throws QueryException {
        if (conn == null) {
            throw new QueryException("no connection - cannot find edges");
        }
        List<Edge> edges = new ArrayList<>();
        try {
            int vertexId = getVertexNumber(destId);
            String sql = String.format("SELECT edge.id, edge_type.type, edge.name, ns1.namespace, source, vertex.name, ns2.namespace " +
                                       "FROM edge " +
                                       "JOIN edge_type ON edge.type=edge_type.id " +
                                       "JOIN namespace AS ns1 ON edge.namespace=ns1.id " +
                                       "JOIN vertex ON edge.source=vertex.id " +
                                       "JOIN namespace AS ns2 ON vertex.namespace=ns2.id " +
                                       "WHERE destination=%d", vertexId);
            try (Statement stmt = conn.createStatement();
                 ResultSet rs = stmt.executeQuery(sql)) {
                while (rs.next()) {
                    // H2 and Derby have the column labels as the original column names
                    // (e.g. namespace, not ns1.namespace), so use column indices
                    String edgeType = rs.getString("edge_type.type");
                    QName edgeId = new QName(rs.getString(4), rs.getString("edge.name"));
                    QName sourceId = new QName(rs.getString(7), rs.getString("vertex.name"));
                    Edge edge = edge(edgeType, edgeId, sourceId, destId);
                    getEdgeAttributes(rs.getInt("edge.id"), edge);
                    edges.add(edge);
                }
            }
        } catch (SQLException e) {
            throw new QueryException(e);
        }
        return edges;
    }
    
    public Graph connected(QName vertexId) throws QueryException {
        return ancestors(vertexId).union(descendants(vertexId));
    }
    
    public Graph connected(List<QName> vertexIds) throws QueryException {
        Graph graph = new Graph();
        for (QName vertexId : vertexIds) {
            graph = graph.union(connected(vertexId));
        }
        return graph;
    }
    
    public Graph graph() throws QueryException {
        if (conn == null) {
            throw new QueryException("no connection - cannot find vertices");
        }
        
        Graph graph = new Graph();
        
        // Get edges 
        String sql = "SELECT edge.name, edge.id, edge_type.type, edge.source, edge.destination, namespace.namespace FROM edge " +
               "JOIN namespace ON edge.namespace=namespace.id " +
               "JOIN edge_type ON edge.type=edge_type.id";
        Edge edge = null;
        try {
            try (Statement stmt = conn.createStatement();
                 ResultSet rs = stmt.executeQuery(sql)) {
                while (rs.next()) {
                    QName edgeId = new QName(rs.getString("namespace.namespace"), rs.getString("edge.name"));
                    String edgeType = rs.getString("edge_type.type");
                    QName source = null;
                    QName destination = null;
                    try {
                        source = getVertexId(rs.getInt("edge.source"));
                        destination = getVertexId(rs.getInt("edge.destination"));
                    } catch (SQLException e) {
                        logger.debug("Edge with invalid source/destination found", e);
                        continue;
                    }
                    if (source == null || destination == null) {
                        continue;
                    }
                    int edgeNumber = rs.getInt("edge.id");
                    edge = edge(edgeType, edgeId, source, destination);
                    getEdgeAttributes(edgeNumber, edge);
                    graph.add(edge);
                }
            }
        } catch (SQLException e) {
            throw new QueryException(e);
        }
        
        // Get vertices 
        sql = "SELECT vertex.name, vertex.id, vertex_type.type, namespace.namespace FROM vertex " +
                        "JOIN namespace ON vertex.namespace=namespace.id " +
                        "JOIN vertex_type ON vertex_type.id=vertex.type";
        Vertex vertex = null;
        try {
            try (Statement stmt = conn.createStatement();
                 ResultSet rs = stmt.executeQuery(sql)) {
                while (rs.next()) {
                    QName vertexId = new QName(rs.getString("namespace.namespace"), rs.getString("vertex.name"));
                    int vertexNumber = rs.getInt("vertex.id");
                    String vertexType = rs.getString("type");
                    if ("activity".equals(vertexType)) {
                        vertex = new Activity(vertexId);
                    } else if ("agent".equals(vertexType)) {
                        vertex = new Agent(vertexId);
                    } else if ("entity".equals(vertexType)) {
                        vertex = new Entity(vertexId);
                    } else {
                        throw new QueryException(String.format("unknown vertex type: %s", vertexType));
                    }
                    getVertexAttributes(vertexNumber, vertex);
                    graph.add(vertex);
                }
            }
        } catch (SQLException e) {
            throw new QueryException(e);
        }
        
        return graph;
    }

    public Graph subgraph(QName vertexId) throws QueryException {
        return subgraph(Collections.singletonList(vertexId));
    }
    public Graph subgraph(QName vertexId, int hops) throws QueryException {
        return subgraph(Collections.singletonList(vertexId), hops);
    }
    
    public Graph subgraph(List<QName> vertexIds) throws QueryException {
        return subgraph(vertexIds, DEFAULT_HOPS);
    }
    public Graph subgraph(List<QName> vertexIds, int hops) throws QueryException {
        Graph graph = new Graph();
        for (QName vertexId : vertexIds) {
            subgraph(graph, vertexId, hops);
        }
        
        return graph;
    }
    public void subgraph(Graph graph, QName vertexId, int hops) throws QueryException {
        if (hops < 0) {
            // can lead to unconnected edges 
            return;
        }
        if (graph.vertices.containsKey(vertexId)) {
            return;
        }
        
        try {
            Vertex v = getVertex(vertexId);
            graph.add(v);
        } catch (QueryException e) {
            logger.debug("Edge with invalid source/destination found", e);
            return;
        }
        
        List<Edge> edges = outgoing(vertexId);
        // don't follow vertices w. too many children 
        //TODO: sub w. cluster placeholder?
        if (edges.size() > MAX_CHILDREN_FOLLOW) {
            edges.clear();
        }
        for(Edge edge : edges) {
            if (edge == null) {
                continue;
            }
            graph.add(edge);
            QName vId = edge.getDestination();
            if (!graph.vertices.containsKey(vId)) {
                subgraph(graph, vId, hops-1);
            }
        }
        
        edges = incoming(vertexId);
        // don't follow vertices w. too many children 
        //TODO: sub w. cluster placeholder?
        if (edges.size() > MAX_CHILDREN_FOLLOW) {
            edges.clear();
        }
        for(Edge edge : edges) {
            if (edge == null) {
                continue;
            }
            graph.add(edge);
            QName vId = edge.getSource();
            if (!graph.vertices.containsKey(vId)) {
                subgraph(graph, vId, hops-1);
            }
        }
    }
    
}
